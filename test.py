import sqlite3

def vulnerable_query(user_input):
    # Connect to the SQLite database
    connection = sqlite3.connect('example.db')
    cursor = connection.cursor()
    
    # Create a table if it doesn't exist
    cursor.execute('CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username TEXT, password TEXT)')
    
    # Vulnerable SQL query
    query = "SELECT * FROM users WHERE username = '" + user_input + "'"
    
    # Executing the vulnerable SQL query
    cursor.execute(query)
    results = cursor.fetchall()
    
    # Printing the results
    for row in results:
        print(row)
    
    # Close the database connection
    connection.close()

# Example of user input that would lead to SQL injection
user_input = "admin' --"
vulnerable_query(user_input)
